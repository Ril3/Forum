import express from 'express';
import cors from 'cors';
import { userController } from './controller/user-controller';
import { messageController } from './controller/message-controller';
import { topicController } from './controller/topic-controller';

export const server = express();

server.use(express.json());
server.use(cors());

server.use(userController);
server.use(messageController);
server.use(topicController)

server.use('/api/forum',userController);
server.use('/api/forum/message', messageController);
server.use('/api/forum/topic',topicController);

