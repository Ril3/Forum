import { Message } from '../entity/Message';
import { connection } from './connection';


//Méthode findMessageById
export async function findMessageById(id) {
    const [rows] = await connection.execute('SELECT * FROM message WHERE id=?', [id]);
    if (rows.length === 1) {
        return new Message(rows[0].author, rows[0].contenu, rows[0].date, rows[0].id)
    }
    return null;
}
// Methode ADD message 

export async function addMessage(message) {
    await connection.execute('INSERT INTO message (author, contenu, date) VALUES (?,?,?)', [message.author, message.contenu, message.date]);
}


//Méthode Update
export async function updateMessage(parMessage){
    const [rows] = await connection.query('UPDATE message SET author=?, contenu=?, date=? WHERE id=?', [parMessage.author, parMessage.contenu, parMessage.date,parMessage.id])

}
//Methode delete
export async function deleteMessage(id){
    const [rows] = await connection.execute('DELETE FROM message WHERE id=?', [id]);
}