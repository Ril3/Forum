import { User } from '../entity/User';
import { connection } from './connection';


/**
 * Fonction qui fait une requête SQL avec mysql2 et transforme le 
 * résultat de 
 */
export async function findAllUsersQuery() {


    const [rows] = await connection.execute('SELECT * FROM user');
    
    const users = [];
    for (const row of rows) {
        let instance = new User(row.name, row.firstName, row.age,row.id, row.rights);
        users.push(instance);

    }
    return users;
}
 

 // Find user by id

/**
 * 
 * @param {Number} paramId 
 * @returns {Promise<Student>}
 */
 export async function findOneUser(id){
    const [rows] = await connection.execute('SELECT * FROM user WHERE id=?', [id]);
    if(rows.length === 1){
        return new User(rows[0].name, rows[0].firstName, rows[0].age,rows[0].id,rows[0].rights)
    }
    return null;
    
    
    }




    // Update methode (This methode insert user in database)

    export async function updateUser(parUser){
        
        const [rows] = await connection.execute('UPDATE user SET name=?, firstName=?, age=?, rights=? WHERE id=?', [parUser.name, parUser.firstName, parUser.age, parUser.rights, parUser.id])

    }


    // Delete methode

    export async function deleteUser(id){
        const [rows] = await connection.execute('DELETE FROM user WHERE id=?', [id]);
    }