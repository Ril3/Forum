import { Topic } from "../entity/Topic";
import { connection } from "./connection";

// FindTopicById

export async function findTopicById(id){
    const[rows] = await connection.execute('SELECT * FROM topic WHERE id=?', [id]);
    if(rows.length === 1){
        return new Topic(rows[0].title, rows[0].description, rows[0].author, rows[0].date, rows[0].id);
    }
    return null;
} 


//update Topic
export async function updateTopic(parTopic){
    const [rows] = await connection.query('UPDATE topic SET title=?, description=?, author=?,date=? WHERE id=?', [parTopic.title, parTopic.description, parTopic.author,parTopic.date,parTopic.id])

}
// delete Topic
export async function deleteTopic(id){
    const [rows] = await connection.query('DELETE FROM topic WHERE id=?', [id]);
} 