import {Router} from "express";
import { deleteTopic, findTopicById, updateTopic } from "../reposetory/topicRepository";

//Topic update
export const topicController = Router();
topicController.put('/api/forum/topic/update', async (req,resp)=>{
    await updateTopic(req.body);
    resp.end();
})

//Topic Delete
topicController.delete('/api/forum/topic/delete/:id',async (req,resp)=>{
    try{
        await deleteTopic(req.params.id);
        resp.status(204).end()
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }

})

// Find Topic by Id

topicController.get('/api/forum/topic/find/:id', async (req, resp) => {
    let topic = await findTopicById(req.params.id);
    if(!topic){ 
        resp.status(404).json ({error : 'Pas trouvé'});  
        return;   
      }
      resp.json(topic);
})  