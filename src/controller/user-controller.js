import {Router} from "express";
import { findAllUsersQuery, findOneUser, updateUser, deleteUser } from "../reposetory/userReposetory";

export const userController = Router();


// Here we have get methode who call function findAllUsersQuery

userController.get('/api/forum/all',async (req,resp)=>{
    let user = await findAllUsersQuery();
    resp.json(user)
});



// Here we have get methode who call function findOneUser

userController.get('/api/forum/one/:id', async(req,resp)=>{
    let user = await findOneUser(req.params.id);
    if(!user){
        resp.status(404).json ({error : 'Pas trouvé'});
        return;
    }
    resp.json(user);
})


// Here we need methode who call update methode

userController.put('/api/forum/add', async (req, res)=>{
    /// here we will call function updateUser(upUser)
    await updateUser(req.body);
    res.end();
})

// Here we need methode who call delete methode

userController.delete('/api/forum/delete/:id',async (req,resp)=>{
    try{
        await deleteUser(req.params.id);
        resp.status(204).end()
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }

})
