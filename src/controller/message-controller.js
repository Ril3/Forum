import {Router} from "express";
import { addMessage, deleteMessage, findMessageById, updateMessage } from "../reposetory/messageRepository";

export const messageController = Router();
//Méthode Update
messageController.put('/api/forum/message/update', async (req,resp)=>{
    await updateMessage(req.body);
    resp.end();
})


//Méthode delete
messageController.delete('/api/forum/delete/message/:id',async (req,resp)=>{
    try{
        await deleteMessage(req.params.id);
        resp.status(204).end()
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }

})

// Find message by id is HERE!!!!

messageController.get('/api/forum/message/:id', async (req, resp) =>{
  let message = await findMessageById(req.params.id)
  if(!message){ 
  resp.status(404).json ({error : 'Pas trouvé'});  
  return;   
}

  resp.json(message);
} );


// Add message is HERE!!!

messageController.post('/add', async (req, res)=>{
  await addMessage(req.body);
  res.end();
})