export class Message {
    id;
    author;
    contenu;
    date;

/**
 * 
 * @param {String} author 
 * @param {String} contenu 
 * @param {Date} date 
 * @param {Number} id 
 */
constructor(author, contenu, date, id= null) {
    this.author = author;
    this.contenu= contenu;
    this.date= date;
    this.id= id;
    }
}
