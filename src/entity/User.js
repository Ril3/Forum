export class User{
    id;
    name;
    firstName;
    age;

    /**
     * This constructor create object User
     * @param {String} name 
     * @param {String} firstName 
     * @param {Number} age 
     * @param {Number} id 
     */
    constructor(name, firstName, age, id = null ){
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.age = age;

    }
}