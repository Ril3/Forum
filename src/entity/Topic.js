export class Topic{
    id;
    title;
    description;
    author;
    date;

    /**
     * 
     * @param {String} title 
     * @param {String} description 
     * @param {String} author 
     * @param {Date} date 
     * @param {Number} id 
     */
    constructor(title, description, author, date, id=null){
        this.title= title;
        this.description= description;
        this.author= author;
        this.date= date;
        this.id= id;
    }
} 