import 'dotenv-flow/config';
import { server } from "./server";
// import { findAllUsersQuery, findOneUser } from "./reposetory/userReposetory";


//Si jamais on trouve une variable d'environnement qui spécifie le port, on l'utilise, sinon on utilise le port 3000
const port = process.env.PORT || 3000;

server.listen(port, () => {
    console.log('listening  http://localhost:'+port)
});

//Ces deux trucs permettent de solutionner le soucis de address already in use à priori
process.on('SIGINT', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});

process.on('exit', () => { 
    console.log("exiting…"); 
    process.exit(0); 
});
