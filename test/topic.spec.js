import { server } from "../src/server";
import request from "supertest";


describe('Test of Topic root', ()=>{

    it('Should return a specific topic on GET with id', async ()=>{
        let response = await request(server)
        .get('/api/forum/topic/find/2')
        .expect(200)

        expect(response.body).toEqual({
            id: 2,
            title: expect.any(String),
            description: expect.any(String),
            author: expect.any(String),
            date: expect.any(String)
        })
    })

})