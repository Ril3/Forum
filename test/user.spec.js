import { server } from "../src/server";
import request from "supertest";

describe('Test of the User root', ()=>{

    it("It should return a table of a users in GET", async()=>{
        let response = await request(server)
                .get('/api/forum/all')
                .expect(200);
              //response have to be the table of the all users in the database
                expect(response.body).toContainEqual({
                    id: expect.any(Number),
                    name: expect.any(String),
                    firstName: expect.any(String),
                    age: expect.any(Number)
                });
        })


        it('should return a specific user on GET with id', async () => {
            let response = await request(server)
                .get('/api/forum/one/3')
                .expect(200);
            
            expect(response.body).toEqual({
                id: 3,
                name: expect.any(String),
                firstName: expect.any(String),
                age: expect.any(Number)
            });
        });
    
        it('should return 404 on GET with unexisting id', async () => {
            await request(server)
                .get('/api/forum/one/9999')
                .expect(404);
            
        });
        

})

