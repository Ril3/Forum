import { server } from "../src/server";
import request from "supertest";

describe('Mesage root', ()=>{

    it('Should return a specific message on GET with id', async ()=>{
        let response = await request(server)
        .get('/api/forum/message/1')
        .expect(200)

        expect(response.body).toEqual({
            id:1,
            author: expect.any(String),
            contenu: expect.any(String),
            date: expect.any(String)
        })
    })
})
