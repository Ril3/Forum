# Projet Forum
Ce projet est un projet de groupe, composé de 3 personnes.
Nous avons eu ce projet en formation de Developpeur Web et Web Mobile.

## Langage utilisé
- JavaScript
- MySQL
  
## Ce qu'on a utilisé dans le JavaScript
- Express server
- Rooter
- Supertest
- Dotenv-flow

## Objectif
1. Faire la conception d'une application forum
2. Créer un script de mise en place de la base de données
3. Créer un script de mise en place de données de tests 
4. Créer un repository par entité

## Organisation 
Tout d'abord nous avons un chef de projet.

Au début, nous avons créer ensemble un dépot Git, puis les branches pour que chacun puisse travaillé de son côté sans casser le code qui marche sur le master.
Puis le chef de projet a partagé nos taches pour qu'on travaille sur notre propre branche. Chaque jour nous avons eu des briefings pour tout rassembler, et vérifier les bugs.

## Gitlab des membres: 
[Riad](https://gitlab.com/Ril3)
[Jehane](https://gitlab.com/ghanemi.jehane.simplon)
[Thanhmy](https://gitlab.com/Thanhmymy)

## Lien Heroku : 
Lien : (https://shielded-crag-55157.herokuapp.com/)

## Maquette : 
![alt text](/src/img/2021-06-22.png)