-- MESSAGE TABLE
DROP TABLE IF EXISTS message;
CREATE TABLE message (
    id INT auto_increment primary key not null,
    author varchar(100),
    contenu varchar (255),
    date date
);

INSERT INTO message (author, contenu, date)
VALUES ('Thanmy', 'Hello tu peux trouver de bons hotels à Lyon sur Booking', '2021-06-18'),
('Jehane', 'Hey je te conseilles un super hotel à Vieux Lyon', '2021-06-18'),
('Riad', 'Merci beaucoup pour vos conseils', '2021-06-18');


-- USER TABLE

DROP TABLE IF EXISTS user;

CREATE TABLE user (
    id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
    name VARCHAR (255),
    firstName VARCHAR  (225), 
    age INT NOT NULL,
    rights VARCHAR  (255)
);

INSERT INTO user (name, firstName, age, rights)
VALUES ('Ghanemi', 'Jehane', 23, 'admin'),
('Quach', 'Thanmy', 19, 'admin'),
('Malkic', 'Riad', 33, 'admin'),
('Demel', 'Jean', 13, 'member'),
('Joly', 'Catherine', 16, 'member');


-- TOPIC TABLE

DROP TABLE IF EXISTS topic;
CREATE TABLE topic (
    id INT auto_increment primary key not null,
    title varchar (100),
    description varchar(500) not null,
    author varchar(100) not null,
    date date
);

INSERT INTO topic (title, description, author, date)
VALUES ('Recherche le meilleur hotel à Lyon', 'Bonjour, je viens en vacances à Lyon et je recherche un hôtel', 'Riad', '2021-06-17'),
('Voyage Mexique', 'Je cherche un hotel 5* à Mexico', 'Jehane', '2021-06-17'),
('Voyage Cannes', 'Je cherche un hotel 5* à Cannes', 'Thanmy', '2021-06-17');







-- -- -- Creation of first table for user
-- -- CREATE TABLE user(  
-- --     id int NOT NULL primary key AUTO_INCREMENT comment 'primary key',
-- --     Name VARCHAR (255),
-- --     FirstName VARCHAR  (225), 
-- --     Age INT NOT NULL 
-- -- ) default charset utf8 comment '';


-- -- CREATE  TABLE admin(
-- --     id int NOT NULL PRIMARY KEY AUTO_INCREMENT comment 'primary key',
-- --     name VARCHAR (255),
-- --     firstName VARCHAR (255),
-- --     age INT NOT NULL 
-- -- )

-- INSERT INTO user (Name, FirstName, Age, rights)
-- VALUES ("Malkic", "Riad", 33, "Admin");

-- INSERT INTO user (Name, FirstName, Age, rights)
-- VALUES ("Quach", "Thanhmy", 19, "Admin");

-- INSERT INTO user (Name, FirstName, Age, rights)
-- VALUES ("Ghanemi", "Jehane", 23, "Admin");


-- DROP TABLE IF EXISTS 'message';
-- CREATE TABLE message
-- (
--     id INT auto_increment primary key not null,
--     author varchar(100),
--     contenu varchar (255),
--     date date,
--     topic_id int(10) default null,
--     foreign key (`topic_id`) references `topic`(`id`),
--      user_id int (10) default null,
--     foreign key (`user_id`) references `user` (`id`),
-- );
-- DROP TABLE IF EXISTS 'topic'
-- CREATE TABLE topic (
--     id INT auto_increment primary key not null,
--     title varchar (100),
--     description varchar(500) not null,
--     author varchar(100) not null,
--     date date,
--     user_id int (10) default null, 
--     foreign key (`user_id`) references `user` (`id`),
    
-- );
-- INSERT INTO message (author, contenu, date, topic_id, user_id)
-- VALUES('Thanmy', "Hello tu peux trouver des bons hotels a lyon sur booking", '2021-06-09', topic_id, user_id);
-- INSERT INTO message (author, contenu, date, topic_id, user_id) 
-- VALUES ('Jehane', 'TEST', '2021-06-09', topic_id, 3 );